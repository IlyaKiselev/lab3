#include "RandomGamer.h"

/*------------------------------------------------------- RANDOM GAMER --------------------------------------------------------*/

pair<int, int> RandomGamer::Attack()
{
	size_t x, y;

	x = rand() % MAP_SIZE;
	y = rand() % MAP_SIZE;

	while (attackMap[x][y] != Water && attackMap[x][y] != NearTheShip)
	{
		x = rand() % MAP_SIZE;
		y = rand() % MAP_SIZE;
	}

	return make_pair(x, y);
}

int(&RandomGamer::GetMap())[MAP_SIZE + 1][MAP_SIZE + 1]
{
	return ownMap.GetMap();
}

vector<pair<int, int>>& RandomGamer::GetShips()
{
	return ownMap.ships;
}

void RandomGamer::SetTouchdown(const bool value)
{
	refereeTouchdown = value;
}

void RandomGamer::SetWasInjured(const bool value)
{
	refereeWasKilled = value;
}