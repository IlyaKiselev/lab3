#pragma once
#include "BasicInformation.h"

void DrawMap(const int(&input)[MAP_SIZE + 1][MAP_SIZE + 1]);
void DrawPVP(int(&input1)[MAP_SIZE + 1][MAP_SIZE + 1], int(&input2)[MAP_SIZE + 1][MAP_SIZE + 1]);

void CloseBlocks(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1]);

void ArrayCpy(int(&to)[MAP_SIZE + 1][MAP_SIZE + 1], int(&from)[MAP_SIZE + 1][MAP_SIZE + 1]);

bool CheckAround(int(&input1)[MAP_SIZE + 1][MAP_SIZE + 1], int(&input2)[MAP_SIZE + 1][MAP_SIZE + 1]);
bool CheckWater(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1]);
bool CheckShips(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1]);

size_t NameHash(const string input);

void Attach(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1], int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1]);
void FindShips(int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1], vector<pair<int, int>>& ships);
void InitAttackCoord(vector<pair<int, int>>& attackCoord);

void Pick(string inputName, int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1]);
void AutoPick(string inputName, int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1]);
void RandomPick(string inputName, int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1]);