#pragma once
#include "BasicInformation.h"
#include "Functions.h"
#include "MainClasses.h"

class OptimalGamer : public IGamer
{
public:
	OptimalGamer(string inputName)
	{
		name = inputName;
		AutoPick(name, ownMap.GetMap());
		FindShips(ownMap.GetMap(), ownMap.ships);
		InitAttackCoord(attackCoord);
	}

	pair<int, int> Attack() override;

	int(&GetMap())[MAP_SIZE + 1][MAP_SIZE + 1] override;
	int(&GetAttackMap())[MAP_SIZE + 1][MAP_SIZE + 1] override;
	vector<pair<int, int>>& GetShips() override;

	void SetTouchdown(const bool value) override;
	void SetWasInjured(const bool value) override;

private:
	Table ownMap;
	int attackMap[MAP_SIZE + 1][MAP_SIZE + 1] = {};
	vector<pair<int, int>> enemyShips;
	bool refereeWasKilled = true;
	bool refereeTouchdown = false;
	bool wasKilled = true;
	bool touchdown = false;
	vector<pair<int, int>> around;
	vector<pair<int, int>> attackCoord;
	pair<int, int> lastCoord;
	string name = "Optimal Player";
	string type = "optimal";
};