#include "ConsoleGamer.h"

/*------------------------------------------------------- CONSOLE GAMER --------------------------------------------------------*/

pair<int, int> ConsoleGamer::Attack()
{
	int x, y;

	cout << "Enter coordinates: ";
	cin >> y >> x;

	if ((x < 0 || x > 9) || (y < 0 || y > 9) || (attackMap[x][y] != Water && attackMap[x][y] != NearTheShip))
	{
		while ((x < 0 || x > 9) || (y < 0 || y > 9) || (attackMap[x][y] != Water && attackMap[x][y] != NearTheShip))
		{
			cout << "Bad input!!!" << endl;
			cout << "Enter coordinates: ";
			cin >> y >> x;
		}
	}

	return make_pair(x, y);
}

int(&ConsoleGamer::GetMap())[MAP_SIZE + 1][MAP_SIZE + 1]
{
	return ownMap.GetMap();
}

int(&ConsoleGamer::GetAttackMap())[MAP_SIZE + 1][MAP_SIZE + 1]
{
	return attackMap;
}

vector<pair<int, int>>& ConsoleGamer::GetShips()
{
	return ownMap.ships;
}

void ConsoleGamer::SetTouchdown(const bool value)
{
	refereeTouchdown = value;
}

void ConsoleGamer::SetWasInjured(const bool value)
{
	refereeWasKilled = value;
}