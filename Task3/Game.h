#pragma once
#include "MainClasses.h"
#include "ConsoleGamer.h"
#include "RandomGamer.h"
#include "OptimalGamer.h"

class Game
{
public:
	Game(int argc, char* argv[]);

	void Process();

private:
	size_t countOfRounds = 1;
	size_t score1 = 0;
	size_t score2 = 0;
	string player1Type = "random";
	string player2Type = "random";
	IGamer* i1;
	IGamer* i2;
};