#include "MainClasses.h"

/*------------------------------------------------------- TABLE (�����) --------------------------------------------------------*/

void Table::Draw() const
{
	DrawMap(mainMap);
}

int(&Table::GetMap())[MAP_SIZE + 1][MAP_SIZE + 1]
{
	return mainMap;
}

void Table::SetMap(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1])
{
	ArrayCpy(mainMap, input);
}

/*------------------------------------------------------- REFEREE --------------------------------------------------------*/

bool Referee::Attack() // �������� ������� �����
{
	alreadyUsedSee = false;
	alreadyUsedCheck = false;
	alreadyUsedCheckInjure = false;

	if (first)
	{
		pair<int, int> tmp = i1->Attack();
		
		if (i2->GetMap()[tmp.first][tmp.second] == Ship)
		{
			touchdown = true;

			if (CheckInjure(tmp))
			{
				wasKilled = false;
			}
			else
			{
				wasKilled = true;
			}
		}
		else
		{
			touchdown = false;
		}
		
		Check(tmp);
		lastWasFirst = true;
		i1->SetTouchdown(touchdown);
		i1->SetWasInjured(wasKilled);
		return true;
	}
	else
	{
		pair<int, int> tmp = i2->Attack();
		
		if (i1->GetMap()[tmp.first][tmp.second] == Ship)
		{
			touchdown = true;
			
			if (CheckInjure(tmp))
			{
				wasKilled = false;
			}
			else
			{
				wasKilled = true;
			}
		}
		else
		{
			touchdown = false;
		}

		Check(tmp);
		lastWasFirst = false;
		i2->SetTouchdown(touchdown);
		i2->SetWasInjured(wasKilled);
		return false;
	}
}

bool Referee::Check(pair<int, int> input) //�������� � ����� �� ��������� ����������, � ������ ��������� ���������� true
{
	if (!alreadyUsedCheck)
	{
		alreadyUsedCheck = true;
		int x = input.first;
		int y = input.second;

		if (first)
		{
			if (i2->GetMap()[x][y] == Ship)
			{
				alreadyUsedCheck = false;
				i2->GetMap()[x][y] = DestroyedShip;
				i1->GetAttackMap()[x][y] = DestroyedShip;
				CloseBlocks(i2->GetMap());
				CloseBlocks(i1->GetAttackMap());
				first = true;
				return true;
			}
			else
			{
				alreadyUsedCheck = true;
				i2->GetMap()[x][y] = AlreadyAttacked;
				i1->GetAttackMap()[x][y] = AlreadyAttacked;
				CloseBlocks(i2->GetMap());
				CloseBlocks(i1->GetAttackMap());
				first = false;
				return false;
			}
		}
		else
		{
			if (i1->GetMap()[x][y] == Ship)
			{
				alreadyUsedCheck = false;
				i1->GetMap()[x][y] = DestroyedShip;
				i2->GetAttackMap()[x][y] = DestroyedShip;
				CloseBlocks(i1->GetMap());
				CloseBlocks(i2->GetAttackMap());
				first = false;
				return true;
			}
			else
			{
				alreadyUsedCheck = true;
				i1->GetMap()[x][y] = AlreadyAttacked;
				i2->GetAttackMap()[x][y] = AlreadyAttacked;
				CloseBlocks(i1->GetMap());
				CloseBlocks(i2->GetAttackMap());
				first = true;
				return false;
			}
		}
	}
}

bool Referee::CheckInjure(pair<int, int> input) //�������� �� ����������� ��� ����������� ������� �� ��������� �����������, ���������� true � ������ �����������
{
	if (!alreadyUsedCheckInjure)
	{
		alreadyUsedCheckInjure = true;
		int x = input.first;
		int y = input.second;
		int counterForBlocks = 0;

		if (first)
		{
			if (i2->GetMap()[x][y] == Ship)
			{
				auto current = find(ships2.begin(), ships2.end(), make_pair(x, y));
				alreadyUsedCheckInjure = false;

				if (current != ships2.end())
				{
					*current = make_pair(-2, -2);

					for (int sub = -2; sub != 3; ++sub)
					{
						if (current._Ptr + sub != nullptr)
						{
							if (*(current + sub) != make_pair(-1, -1) && *(current + sub) != make_pair(-2, -2))
								++counterForBlocks;
						}
					}
				}
			}
		}
		else
		{
			if (i1->GetMap()[x][y] == Ship)
			{
				auto current = find(ships1.begin(), ships1.end(), make_pair(x, y));
				alreadyUsedCheckInjure = false;

				if (current != ships1.end())
				{
					*current = make_pair(-2, -2);

					for (int sub = -2; sub != 3; ++sub)
					{
						if (current._Ptr + sub != nullptr)
						{
							if (*(current + sub) != make_pair(-1, -1) && *(current + sub) != make_pair(-2, -2))
								++counterForBlocks;
						}
					}
				}
			}
		}

		if (counterForBlocks <= 0)
		{
			if (!alreadyUsedCheckInjure)
			{
				cout << "Destroyed!" << endl;
				return false;
			}
		}
		else
		{
			if (!alreadyUsedCheckInjure)
			{
				cout << "Injured!" << endl;
				return true;
			}
		}
	}
	
	return false;
}

bool Referee::WasKilled() const
{
	return wasKilled;
}

bool Referee::Touchdown() const
{
	return touchdown;
}