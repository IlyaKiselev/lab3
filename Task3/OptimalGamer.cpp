#include "OptimalGamer.h"

/*------------------------------------------------------- OPTIMAL GAMER --------------------------------------------------------*/

pair<int, int> OptimalGamer::Attack()
{
	int x, y;

	x = rand() % MAP_SIZE;
	y = rand() % MAP_SIZE;

	if (CheckWater(attackMap))
	{
		while (attackMap[x][y] != Water)
		{
			x = rand() % MAP_SIZE;
			y = rand() % MAP_SIZE;
		}
	}
	else
	{
		while (attackMap[x][y] != NearTheShip)
		{
			x = rand() % MAP_SIZE;
			y = rand() % MAP_SIZE;
		}
	}
	
	if (refereeTouchdown)
	{
		x = lastCoord.first;
		y = lastCoord.second;

		if (refereeWasKilled)
		{
			wasKilled = true;
			touchdown = false;
			around.clear();
		}
		else
		{
			if (wasKilled)
			{
				around.push_back(make_pair(x, y - 3));
				around.push_back(make_pair(x, y - 2));
				around.push_back(make_pair(x, y - 1));
				around.push_back(make_pair(-1, -1));
				around.push_back(make_pair(x, y + 3));
				around.push_back(make_pair(x, y + 2));
				around.push_back(make_pair(x, y + 1));
				around.push_back(make_pair(-1, -1));
				around.push_back(make_pair(x - 3, y));
				around.push_back(make_pair(x - 2, y));
				around.push_back(make_pair(x - 1, y));
				around.push_back(make_pair(-1, -1));
				around.push_back(make_pair(x + 3, y));
				around.push_back(make_pair(x + 2, y));
				around.push_back(make_pair(x + 1, y));
			}
			touchdown = true;
			wasKilled = false;
		}
	}
	else
	{
		touchdown = false;
	}

	if (wasKilled)
	{
		if (!attackCoord.empty())
		{
			x = attackCoord.back().first;
			y = attackCoord.back().second;
			attackCoord.pop_back();
			lastCoord = make_pair(x, y);

			if (attackMap[x][y] != Water)
			{
				while (attackMap[x][y] != Water)
				{
					if (!attackCoord.empty())
					{
						x = attackCoord.back().first;
						y = attackCoord.back().second;
						attackCoord.pop_back();
						lastCoord = make_pair(x, y);
					}
					else
						break;
				}
			}
		}
	}
	else
	{
		if (!around.empty())
		{
			if (!touchdown)
			{
				while (around.back().first != -1 && around.back().second != -1)
				{
					around.pop_back();
				}
			}

			if (!around.empty())
			{
				x = around.back().first;
				y = around.back().second;
				around.pop_back();
				lastCoord = make_pair(x, y);
			}

			if (x < 0 || x > 9 || y < 0 || y > 9)
			{
				while (x < 0 || x > 9 || y < 0 || y > 9)
				{
					if (!around.empty())
					{
						x = around.back().first;
						y = around.back().second;
						around.pop_back();
						lastCoord = make_pair(x, y);
					}
				}
			}
		}
	}

	return make_pair(x, y);
}

int(&OptimalGamer::GetMap())[MAP_SIZE + 1][MAP_SIZE + 1]
{
	return ownMap.GetMap();
}

int(&OptimalGamer::GetAttackMap())[MAP_SIZE + 1][MAP_SIZE + 1]
{
	return attackMap;
}

vector<pair<int, int>>& OptimalGamer::GetShips()
{
	return ownMap.ships;
}

void OptimalGamer::SetTouchdown(const bool value)
{
	refereeTouchdown = value;
}

void OptimalGamer::SetWasInjured(const bool value)
{
	refereeWasKilled = value;
}