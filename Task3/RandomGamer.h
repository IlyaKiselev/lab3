#include "BasicInformation.h"
#include "Functions.h"
#include "MainClasses.h"

class RandomGamer : public IGamer
{
public:
	RandomGamer(string inputName)
	{
		name = inputName;
		RandomPick(name, ownMap.GetMap());
		FindShips(ownMap.GetMap(), ownMap.ships);
	}

	pair<int, int> Attack() override;

	int(&GetMap())[MAP_SIZE + 1][MAP_SIZE + 1] override;
	int(&GetAttackMap())[MAP_SIZE + 1][MAP_SIZE + 1] override{ return attackMap; }
	vector<pair<int, int>>& GetShips() override;
	
	void SetTouchdown(const bool value) override;
	void SetWasInjured(const bool value) override;

private:
	Table ownMap;
	int attackMap[MAP_SIZE + 1][MAP_SIZE + 1] = {};
	vector<pair<int, int>> enemyShips;
	bool refereeWasKilled = true;
	bool refereeTouchdown = false;
	bool wasKilled = true;
	bool touchdown = false;
	vector<pair<int, int>> around;
	vector<pair<int, int>> attackCoord;
	string name = "Random Player";
	string type = "random";
};