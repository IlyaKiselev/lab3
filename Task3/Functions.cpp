#include "Functions.h"

void DrawMap(const int(&input)[MAP_SIZE + 1][MAP_SIZE + 1])//������� ������ ������ ����, ����� ��� �������������� ������� ������ ��������
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));
	std::cout << "            0 1 2 3 4 5 6 7 8 9             " << endl;
	SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		if (i % 2 == 0)
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));
			std::cout << "          " << i << " ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}
		else
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));
			std::cout << "          " << i << " ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}

		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if (input[i][j] == Water)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
				std::cout << "~" << " ";
			}

			if (input[i][j] == Ship)	
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::LightGreen));
				std::cout << "#" << " ";
			}

			if (input[i][j] == DestroyedShip)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Red));
				std::cout << "X" << " ";
			}

			if (input[i][j] == NearTheShip)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Blue));
				std::cout << "~" << " ";
			}

			if (input[i][j] == BadPosition)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Yellow));
				std::cout << "#" << " ";
			}

			if (input[i][j] == AlreadyAttacked)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Yellow));
				std::cout << "0" << " ";
			}

			SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 7));
		}
		if (i % 2 == 0)
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));
			std::cout << "            ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}
		else
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));
			std::cout << "            ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}
		std::cout << endl;
	}

	SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));
	std::cout << "                                            " << endl;
	SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
}

void DrawPVP(int(&input1)[MAP_SIZE + 1][MAP_SIZE + 1], int(&input2)[MAP_SIZE + 1][MAP_SIZE + 1]) //������� ������ ����� ���� �������
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));
	std::cout << "            0 1 2 3 4 5 6 7 8 9                    " << "                   0 1 2 3 4 5 6 7 8 9             " << endl;
	SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		if (i % 2 == 0)
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));
			std::cout << "          " << i << " ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}
		else
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));
			std::cout << "          " << i << " ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}

		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if (input1[i][j] == Water)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
				std::cout << "~" << " ";
			}

			if (input1[i][j] == Ship)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::LightGreen));
				std::cout << "#" << " ";
			}

			if (input1[i][j] == DestroyedShip)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Red));
				std::cout << "X" << " ";
			}

			if (input1[i][j] == NearTheShip)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Blue));
				std::cout << "~" << " ";
			}

			if (input1[i][j] == BadPosition)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Yellow));
				std::cout << "#" << " ";
			}

			if (input1[i][j] == AlreadyAttacked)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Yellow));
				std::cout << "0" << " ";
			}

			SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 7));
		}
		if (i % 2 == 0)
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));
			std::cout << "            ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}
		else
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));
			std::cout << "            ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}

		if (i % 2 == 0)
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));
			std::cout << "                        " << i << " ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}
		else
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));
			std::cout << "                        " << i << " ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}

		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if (input2[i][j] == Water)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
				std::cout << "~" << " ";
			}

			if (input2[i][j] == Ship)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::LightGreen));
				std::cout << "#" << " ";
			}

			if (input2[i][j] == DestroyedShip)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Red));
				std::cout << "X" << " ";
			}

			if (input2[i][j] == NearTheShip)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Blue));
				std::cout << "~" << " ";
			}

			if (input2[i][j] == BadPosition)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Yellow));
				std::cout << "#" << " ";
			}

			if (input2[i][j] == AlreadyAttacked)
			{
				SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::Yellow));
				std::cout << "0" << " ";
			}

			SetConsoleTextAttribute(hConsole, (WORD)((0 << 4) | 7));
		}
		if (i % 2 == 0)
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));
			std::cout << "            ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}
		else
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));
			std::cout << "            ";
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
		}
		std::cout << endl;
	}

	SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));
	std::cout << "                                                                                                      " << endl;
	SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Black << 4) | ConsoleColor::White));
}

void CloseBlocks(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1]) //�������, ������� �������� ������ ������ ����������� �������� (�� ���� ������� ��� �� ����� ���� ��������)
{
	for (int i = 0; i != MAP_SIZE; ++i)
	{
		for (int j = 0; j != MAP_SIZE; ++j)
		{
			if (input[i][j] == DestroyedShip)
			{
				for (int di = -1; di != 2; ++di)
				{
					for (int dj = -1; dj != 2; ++dj)
					{
						if (input[i + di][j + dj] == 0)
						{
							if (input[i + di][j + dj] == 0)
								input[i + di][j + dj] = NearTheShip;
						}
					}
				}
			}
		}
	}
}

void ArrayCpy(int(&to)[MAP_SIZE + 1][MAP_SIZE + 1], int(&from)[MAP_SIZE + 1][MAP_SIZE + 1])
{
	for (int i = 0; i != MAP_SIZE; ++i)
	{
		for (int j = 0; j != MAP_SIZE; ++j)
		{
			to[i][j] = from[i][j];
		}
	}
}

bool CheckAround(int(&input1)[MAP_SIZE + 1][MAP_SIZE + 1], int(&input2)[MAP_SIZE + 1][MAP_SIZE + 1])
{
	int tmpInput[MAP_SIZE + 1][MAP_SIZE + 1];
	ArrayCpy(tmpInput, input1);

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			tmpInput[i][j] -= input2[i][j];
		}
	}

	for (int i = 0; i != MAP_SIZE; ++i)
	{
		for (int j = 0; j != MAP_SIZE; ++j)
		{
			for (int di = -1; di != 2; ++di)
			{
				for (int dj = -1; dj != 2; ++dj)
				{
					if (tmpInput[i][j] == Ship)
					{
						if (input2[i + di][j + dj] == tmpInput[i][j])
						{
							return false;
						}
					}	
				}
			}
		}
	}
	return true;
}

bool CheckWater(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1])
{
	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if (input[i][j] == Water)
				return true;
		}
	}
	return false;
}

bool CheckShips(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1])
{
	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if (input[i][j] == Ship)
				return true;
		}
	}
	return false;
}

size_t NameHash(const string input)
{	
	size_t result = 0;
	size_t div = 0;
	size_t counter = 0;
	
	for (auto it : input)
	{
		++counter;
		size_t div = rand();
		result += (it) * counter;
	}

	return result;
}

void Attach(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1], int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1])
{
	size_t x, y;
	size_t countOfShips = 0;

	int tmp[MAP_SIZE + 1][MAP_SIZE + 1];

	ArrayCpy(mainMap, input);

	while (countOfShips != 4)
	{
		ArrayCpy(tmp, input);

		x = rand() % MAP_SIZE;
		y = rand() % MAP_SIZE;

		if (mainMap[x][y] != 1)
			input[x][y] = 1;

		if (CheckAround(input, mainMap) && mainMap[x][y] != Ship)
		{
			ArrayCpy(mainMap, input);
			++countOfShips;
		}
		else
		{
			ArrayCpy(input, tmp);
		}
	}
}

void FindShips(int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1], vector<pair<int, int>>& ships) //������� ������ �������� �� ���� ��� ���������� �� � ������	
{																						 //����� ��� ��������� � ����������� ��� ���������� �������
	ships.push_back(make_pair(-1, -1));
	ships.push_back(make_pair(-1, -1));

	for (int i = 0; i != MAP_SIZE; ++i)
	{
		for (int j = 0; j != MAP_SIZE; ++j)
		{
			if (mainMap[i][j] == Ship)
			{
				if (find(ships.begin(), ships.end(), make_pair(i, j)) == ships.end())
					ships.push_back(make_pair(i, j));

				if (i + 1 != MAP_SIZE && mainMap[i + 1][j] == Ship)
				{
					if (find(ships.begin(), ships.end(), make_pair(i + 1, j)) == ships.end())
						ships.push_back(make_pair(i + 1, j));

					if (i + 2 != MAP_SIZE && mainMap[i + 2][j] == Ship)
					{
						if (find(ships.begin(), ships.end(), make_pair(i + 2, j)) == ships.end())
							ships.push_back(make_pair(i + 2, j));

						if (i + 3 != MAP_SIZE && mainMap[i + 3][j] == Ship)
						{
							if (find(ships.begin(), ships.end(), make_pair(i + 3, j)) == ships.end())
								ships.push_back(make_pair(i + 3, j));
						}
					}
				}

				if (j + 1 != MAP_SIZE && mainMap[i][j + 1] == Ship)
				{
					if (find(ships.begin(), ships.end(), make_pair(i, j + 1)) == ships.end())
						ships.push_back(make_pair(i, j + 1));

					if (j + 2 != MAP_SIZE && mainMap[i][j + 2] == Ship)
					{
						if (find(ships.begin(), ships.end(), make_pair(i, j + 2)) == ships.end())
							ships.push_back(make_pair(i, j + 2));

						if (j + 3 != MAP_SIZE && mainMap[i][j + 3] == Ship)
						{
							if (find(ships.begin(), ships.end(), make_pair(i, j + 3)) == ships.end())
								ships.push_back(make_pair(i, j + 3));
						}
					}
				}

				if (i - 1 != -1 && mainMap[i - 1][j] == Ship)
				{
					if (find(ships.begin(), ships.end(), make_pair(i - 1, j)) == ships.end())
						ships.push_back(make_pair(i - 1, j));

					if (i - 2 != -1 && mainMap[i - 2][j] == Ship)
					{
						if (find(ships.begin(), ships.end(), make_pair(i - 2, j)) == ships.end())
							ships.push_back(make_pair(i - 2, j));

						if (i - 3 != -1 && mainMap[i - 3][j] == Ship)
						{
							if (find(ships.begin(), ships.end(), make_pair(i - 3, j)) == ships.end())
								ships.push_back(make_pair(i - 3, j));
						}
					}
				}

				if (j - 1 != -1 && mainMap[i][j - 1] == Ship)
				{
					if (find(ships.begin(), ships.end(), make_pair(i, j - 1)) == ships.end())
						ships.push_back(make_pair(i, j - 1));

					if (j - 2 != -1 && mainMap[i][j - 2] == Ship)
					{
						if (find(ships.begin(), ships.end(), make_pair(i, j - 2)) == ships.end())
							ships.push_back(make_pair(i, j - 2));

						if (j - 3 != -1 && mainMap[i][j - 3] == Ship)
						{
							if (find(ships.begin(), ships.end(), make_pair(i, j - 3)) == ships.end())
								ships.push_back(make_pair(i, j - 3));
						}
					}
				}

				ships.push_back(make_pair(-1, -1));
				ships.push_back(make_pair(-1, -1));
			}
		}
	}
	ships.push_back(make_pair(-1, -1));
	ships.push_back(make_pair(-1, -1));
}

// ������� ������ ����������� ��������, ��� ��������� ������ R ��� O ��� ��������� � ����������� 
// ����������� ��������������. ���� ������ ����� ������ �������, �������� ���� ������ �����������: 
// �������� ����������� �������, ������� �������������� ��� ������������ ��������� �� ������,
// Enter - ��������� �������. ���� �� ����� � �� �������� ������� ������� �� ����� ���������.
void Pick(string inputName, int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1]) 
{
	cout << "Choose your strategy: click R for random strategy or O for optimal strategy." << endl << "If you want arrange the ships yourself, click other button" << endl;
	char ch = _getch();

	if (ch == 114 || ch == 82)
	{
		RandomPick(inputName, mainMap);
		std::system("cls");
		return;
	}
	else if (ch == 111 || ch == 79)
	{
		AutoPick(inputName, mainMap);
		std::system("cls");
		return;
	}

	int x0, y0;
	int x1, y1;
	int x2, y2;
	int x3, y3;

	bool isHorizontal;

	int localMap[MAP_SIZE + 1][MAP_SIZE + 1];

	ArrayCpy(localMap, mainMap);

	for (size_t countOfShips = 1; countOfShips != 5; ++countOfShips)
	{
		for (size_t i = countOfShips; i != 0; --i)
		{
			x0 = 0; y0 = 0;
			localMap[x0][y0] = Ship;

			if (countOfShips <= 3)
			{
				x1 = 0; y1 = 1;
				localMap[x1][y1] = Ship;
			}

			if (countOfShips <= 2)
			{
				x2 = 0; y2 = 2;
				localMap[x2][y2] = Ship;
			}

			if (countOfShips <= 1)
			{
				x3 = 0, y3 = 3;
				localMap[x3][y3] = Ship;
			}

			isHorizontal = true;

			DrawMap(localMap);

			while (char c = _getch())
			{
				std::system("cls");

				if (c == 13)
				{
					if (CheckAround(localMap, mainMap))
					{
						ArrayCpy(mainMap, localMap);
						break;
					}
					else
						continue;
				}

				if (c == 32)
				{
					isHorizontal = !isHorizontal;

					if (countOfShips <= 3)
						localMap[x1][y1] = Water;

					if (countOfShips <= 2)
						localMap[x2][y2] = Water;

					if (countOfShips <= 1)
						localMap[x3][y3] = Water;

					if (isHorizontal)
					{
						if ((countOfShips == 3 && x0 <= MAP_SIZE - 2 && y0 <= MAP_SIZE - 2) || (countOfShips == 2 && x0 <= MAP_SIZE - 3 && y0 <= MAP_SIZE - 3) || (countOfShips == 1 && x0 <= MAP_SIZE - 4 && y0 <= MAP_SIZE - 4))
						{
							if (countOfShips <= 3)
							{
								x1--;
								y1++;
							}

							if (countOfShips <= 2)
							{
								x2 -= 2;
								y2 += 2;
							}

							if (countOfShips <= 1)
							{
								x3 -= 3;
								y3 += 3;
							}
						}
						else
							isHorizontal = !isHorizontal;
					}

					if (!isHorizontal)
					{
						if ((countOfShips == 3 && x0 <= MAP_SIZE - 2 && y0 <= MAP_SIZE - 2) || (countOfShips == 2 && x0 <= MAP_SIZE - 3 && y0 <= MAP_SIZE - 3) || (countOfShips == 1 && x0 <= MAP_SIZE - 4 && y0 <= MAP_SIZE - 4))
						{
							if (countOfShips <= 3)
							{
								x1++;
								y1--;
							}

							if (countOfShips <= 2)
							{
								x2 += 2;
								y2 -= 2;
							}

							if (countOfShips <= 1)
							{
								x3 += 3;
								y3 -= 3;
							}
						}
						else
							isHorizontal = !isHorizontal;
					}

					if (countOfShips <= 3)
						localMap[x1][y1] = Ship;

					if (countOfShips <= 2)
						localMap[x2][y2] = Ship;

					if (countOfShips <= 1)
						localMap[x3][y3] = Ship;

					DrawMap(localMap);
				}

				if (c != 32)
				{
					if (mainMap[x0][y0] != Ship)
						localMap[x0][y0] = Water;

					if (mainMap[x1][y1] != Ship)
						localMap[x1][y1] = Water;

					if (mainMap[x2][y2] != Ship)
						localMap[x2][y2] = Water;

					if (mainMap[x3][y3] != Ship)
						localMap[x3][y3] = Water;

					DrawMap(localMap);
				}

				if (isHorizontal)
				{
					if (c == 72 && x0 != 0) // ������� �����
					{
						localMap[--x0][y0] = Ship;

						if (countOfShips <= 3)
							localMap[--x1][y1] = Ship;

						if (countOfShips <= 2)
							localMap[--x2][y2] = Ship;

						if (countOfShips <= 1)
							localMap[--x3][y3] = Ship;
					}

					if (c == 80 && x0 != 9) // ������� ���� 
					{
						localMap[++x0][y0] = Ship;

						if (countOfShips <= 3)
							localMap[++x1][y1] = Ship;

						if (countOfShips <= 2)
							localMap[++x2][y2] = Ship;

						if (countOfShips <= 1)
							localMap[++x3][y3] = Ship;
					}

					if (c == 75 && y0 != 0) // ������� �����
					{
						localMap[x0][--y0] = Ship;

						if (countOfShips <= 3)
							localMap[x1][--y1] = Ship;

						if (countOfShips <= 2)
							localMap[x2][--y2] = Ship;

						if (countOfShips <= 1)
							localMap[x3][--y3] = Ship;
					}

					if (c == 77) // ������� ������ 
					{
						if ((countOfShips == 4 && y0 < MAP_SIZE - 1) || (countOfShips == 3 && y0 < MAP_SIZE - 2) || (countOfShips == 2 && y0 < MAP_SIZE - 3) || (countOfShips == 1 && y0 < MAP_SIZE - 4))
						{
							localMap[x0][++y0] = Ship;

							if (countOfShips <= 3)
								localMap[x1][++y1] = Ship;

							if (countOfShips <= 2)
								localMap[x2][++y2] = Ship;

							if (countOfShips <= 1)
								localMap[x3][++y3] = Ship;
						}
					}
				}
				else
				{
					if (c == 72 && x0 != 0) // ������� �����
					{
						localMap[--x0][y0] = Ship;

						if (countOfShips <= 3)
							localMap[--x1][y1] = Ship;

						if (countOfShips <= 2)
							localMap[--x2][y2] = Ship;

						if (countOfShips <= 1)
							localMap[--x3][y3] = Ship;
					}

					if (c == 80 && x0 != 7) // ������� ���� 
					{
						if ((countOfShips == 3 && x0 < MAP_SIZE - 2) || (countOfShips == 2 && x0 < MAP_SIZE - 3) || (countOfShips == 1 && x0 < MAP_SIZE - 4))
						{
							localMap[++x0][y0] = Ship;

							if (countOfShips <= 3)
								localMap[++x1][y1] = Ship;

							if (countOfShips <= 2)
								localMap[++x2][y2] = Ship;

							if (countOfShips <= 1)
								localMap[++x3][y3] = Ship;
						}
					}

					if (c == 75 && y0 != 0) // ������� �����
					{
						localMap[x0][--y0] = Ship;

						if (countOfShips <= 3)
							localMap[x1][--y1] = Ship;

						if (countOfShips <= 2)
							localMap[x2][--y2] = Ship;

						if (countOfShips <= 1)
							localMap[x3][--y3] = Ship;
					}

					if (c == 77 && y0 != MAP_SIZE - 1) // ������� ������ 
					{
						localMap[x0][++y0] = Ship;

						if (countOfShips <= 3)
							localMap[x1][++y1] = Ship;

						if (countOfShips <= 2)
							localMap[x2][++y2] = Ship;

						if (countOfShips <= 1)
							localMap[x3][++y3] = Ship;
					}
				}

				std::system("cls");
				DrawMap(localMap);

				std::cout << y0 << " " << x0 << endl;
				if (isHorizontal)
					std::cout << "Horizontal" << endl;
				else
					std::cout << "Vertical" << endl;
			}
		}
	}
}

void AutoPick(string inputName, int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1]) //������� ����������� ����������� ��������
{
	int tmpArr[MAP_SIZE + 1][MAP_SIZE + 1]; //������� ������������ ����� � ����������� ������� �������� (�� ����������� ���������, ��� �������� ��������)

	int arr0[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr1[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr2[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr3[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr4[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr5[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr6[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr7[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr8[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int arr9[MAP_SIZE + 1][MAP_SIZE + 1] = {
		{0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
		{0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	};

	int number = rand() % 10;

	switch (number)
	{
	case 0:
		ArrayCpy(tmpArr, arr0);
		break;
	case 1:
		ArrayCpy(tmpArr, arr1);
		break;
	case 2:
		ArrayCpy(tmpArr, arr2);
		break;
	case 3:
		ArrayCpy(tmpArr, arr3);
		break;
	case 4:
		ArrayCpy(tmpArr, arr4);
		break;
	case 5:
		ArrayCpy(tmpArr, arr5);
		break;
	case 6:
		ArrayCpy(tmpArr, arr6);
		break;
	case 7:
		ArrayCpy(tmpArr, arr7);
		break;
	case 8:
		ArrayCpy(tmpArr, arr8);
		break;
	case 9:
		ArrayCpy(tmpArr, arr9);
		break;
	}

	Attach(tmpArr, mainMap);
}

void RandomPick(string inputName, int(&mainMap)[MAP_SIZE + 1][MAP_SIZE + 1]) //������� ��������� ����������� ��������
{
	size_t x, y;
	size_t countOfShips = 0;
	bool isVertical = 0;
	int tmp1[MAP_SIZE + 1][MAP_SIZE + 1];
	int tmp2[MAP_SIZE + 1][MAP_SIZE + 1];


	x = rand() % (10 - 3);
	y = rand() % (10 - 3);
	isVertical = rand() % 2;

	if (isVertical)
	{
		mainMap[x][y] = Ship;
		mainMap[x + 1][y] = Ship;
		mainMap[x + 2][y] = Ship;
		mainMap[x + 3][y] = Ship;
	}
	else
	{
		mainMap[x][y] = Ship;
		mainMap[x][y + 1] = Ship;
		mainMap[x][y + 2] = Ship;
		mainMap[x][y + 3] = Ship;
	}

	ArrayCpy(tmp1, mainMap);
	countOfShips = 0;

	while (countOfShips != 2)
	{
		x = rand() % (10 - 2);
		y = rand() % (10 - 2);
		isVertical = rand() % 2;

		ArrayCpy(tmp2, tmp1);

		if (mainMap[x][y] != Ship)
		{
			if (isVertical)
			{
				tmp1[x][y] = Ship;
				tmp1[x + 1][y] = Ship;
				tmp1[x + 2][y] = Ship;
			}
			else
			{
				tmp1[x][y] = Ship;
				tmp1[x][y + 1] = Ship;
				tmp1[x][y + 2] = Ship;
			}

			if (CheckAround(tmp1, mainMap))
			{
				ArrayCpy(mainMap, tmp1);
				++countOfShips;
			}
			else
			{
				ArrayCpy(tmp1, tmp2);
			}
		}
	}

	ArrayCpy(tmp1, mainMap);
	countOfShips = 0;

	while (countOfShips != 3)
	{
		x = rand() % (10 - 1);
		y = rand() % (10 - 1);
		isVertical = rand() % 2;

		ArrayCpy(tmp2, tmp1);

		if (mainMap[x][y] != Ship)
		{
			if (isVertical)
			{
				tmp1[x][y] = Ship;
				tmp1[x + 1][y] = Ship;
			}
			else
			{
				tmp1[x][y] = Ship;
				tmp1[x][y + 1] = Ship;
			}

			if (CheckAround(tmp1, mainMap))
			{
				ArrayCpy(mainMap, tmp1);
				++countOfShips;
			}
			else
			{
				ArrayCpy(tmp1, tmp2);
			}
		}
	}

	ArrayCpy(tmp1, mainMap);
	Attach(tmp1, mainMap);
}

void InitAttackCoord(vector<pair<int, int>> & attackCoord) // ������� ������������� ����������� ��������� ����� ��� � ������ https://habr.com/ru/post/180995/
{
	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 1)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 5)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 9)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 13)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 17)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 3)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 7)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 11)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}

	for (size_t i = 0; i != MAP_SIZE; ++i)
	{
		for (size_t j = 0; j != MAP_SIZE; ++j)
		{
			if ((i + j) == 15)
				attackCoord.push_back(make_pair((int)i, (int)j));
		}
	}
}