#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <random> 
#include <Windows.h>
#include <conio.h>
#include <vector>
#include <map>
#include <string>

#define MAP_SIZE 10
#define DEBUG_MODE 0 //������� 1, ���� �� ������ ����� ��������� ���� ����� � ����� ������ ������� ����������, ����� ������ ��� ����

enum MapCondition
{
	Water = 0,
	Ship = 1,
	DestroyedShip = 2,
	NearTheShip = 3,
	BadPosition = 4,
	AlreadyAttacked = 5
};

enum ConsoleColor 
{
	Black = 0,
	Blue = 1,
	Green = 2,
	Cyan = 3,
	Red = 4,
	Magenta = 5,
	Brown = 6,
	LightGray = 7,
	DarkGray = 8,
	LightBlue = 9,
	LightGreen = 10,
	LightCyan = 11,
	LightRed = 12,
	LightMagenta = 13,
	Yellow = 14,
	White = 15
};

using namespace std;