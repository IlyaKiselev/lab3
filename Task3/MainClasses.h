#pragma once
#include "BasicInformation.h"
#include "Functions.h"

class Referee;

class Table
{
public:
	void Draw() const;

	int(&GetMap())[MAP_SIZE + 1][MAP_SIZE + 1] ;
	void SetMap(int(&input)[MAP_SIZE + 1][MAP_SIZE + 1]);

public:
	int mainMap[MAP_SIZE + 1][MAP_SIZE + 1] = {};
	vector<pair<int, int>> ships;
};

class IGamer
{
public:
	virtual pair<int, int> Attack() = 0;

	virtual int(&GetMap())[MAP_SIZE + 1][MAP_SIZE + 1] = 0;
	virtual int(&GetAttackMap())[MAP_SIZE + 1][MAP_SIZE + 1] = 0;
	virtual vector<pair<int, int>>& GetShips() = 0;

	virtual void SetTouchdown(const bool value) = 0;
	virtual void SetWasInjured(const bool value) = 0;
};

class Referee
{
public:
	Referee(IGamer* player1, IGamer* player2)
	{
		i1 = player1;
		i2 = player2;
		ships1 = i1->GetShips();
		ships2 = i2->GetShips();
	}
	
	bool Attack();

	bool WasKilled() const;
	bool Touchdown() const;

private:
	bool Check(pair<int, int> input);
	bool CheckInjure(pair<int, int> input);

private:
	bool first = true;
	bool lastWasFirst = true;
	
	bool alreadyUsedSee = false;
	bool alreadyUsedCheck = false;
	bool alreadyUsedCheckInjure = false;
	
	bool touchdown = false;
	bool wasKilled = true;
	
	IGamer* i1 = nullptr;
	IGamer* i2 = nullptr;
	
	vector<pair<int, int>> ships1;
	vector<pair<int, int>> ships2;
};