#include "Game.h"

Game::Game(int argc, char* argv[])
{
	size_t countOfElements = argc;
	vector<string> arguments;

	for (size_t i = 0; i != argc; ++i)
		arguments.push_back(argv[i]);

	for (auto it = arguments.begin(); it != arguments.end(); ++it)
	{
		if (*it == "-f" || *it == "--help")
			player1Type = *(it + 1);

		if (*it == "-s" || *it == "--second")
			player2Type = *(it + 1);

		if (*it == "-c" || *it == "--count")
			countOfRounds = stoi(*(it + 1));
	}

	Process();
}

void Game::Process()
{
	for (size_t i = 0; i != countOfRounds; ++i)
	{
		if (player1Type == "console")
			i1 = new ConsoleGamer("Player1");

		if (player1Type == "random")
			i1 = new RandomGamer("Player1");

		if (player1Type == "optimal")
			i1 = new OptimalGamer("Player1");

		if (player2Type == "console")
			i2 = new ConsoleGamer("Player2");

		if (player2Type == "random")
			i2 = new RandomGamer("Player2");

		if (player2Type == "optimal")
			i2 = new OptimalGamer("Player2");

		Referee r(i1, i2);

		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

		if (player1Type == "console")
			DrawPVP(i1->GetMap(), i1->GetAttackMap());
		else if (player2Type == "console")
			DrawPVP(i1->GetMap(), i1->GetAttackMap());
		else
			DrawPVP(i1->GetMap(), i2->GetMap());


		while (CheckShips(i1->GetMap()) && CheckShips(i2->GetMap()))
		{
			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::White << 4) | ConsoleColor::Black));


			if (r.Attack())
			{
				if (!DEBUG_MODE)
					std::cout << "                                         Player 1 attacks                                             " << endl;
			}
			else
			{
				if (!DEBUG_MODE)
					std::cout << "                                         Player 2 attacks                                             " << endl;
			}

			SetConsoleTextAttribute(hConsole, (WORD)((ConsoleColor::Cyan << 4) | ConsoleColor::Black));

			if (!DEBUG_MODE)
			{
				std::cout << "                                                                                                      " << endl;

				if (player1Type == "console")
					DrawPVP(i1->GetMap(), i1->GetAttackMap());
				else if (player2Type == "console")
					DrawPVP(i1->GetMap(), i1->GetAttackMap());
				else
					DrawPVP(i1->GetMap(), i2->GetMap());

				//DrawPVP(i2->GetAttackMap(), i1->GetAttackMap()); //����� �����, ���� ������ �������� ���� ���� (���� ����������)
			}
		}

		if (CheckShips(i1->GetMap()))
		{
			++score1;
			cout << "Player1 WIN!" << endl;
		}
		else
		{
			++score2;
			cout << "Player2 WIN!" << endl;
		}

		DrawPVP(i1->GetMap(), i2->GetMap());
		delete i1;
		delete i2;
	}

	cout << "TOTAL SCORE: " << score1 << " : " << score2 << endl;

	if (score1 > score2)
		cout << "Player 1 WIN!" << endl;

	if (score1 < score2)
		cout << "Player 2 WIN!" << endl;

	if (score1 == score2)
		cout << "Draw!" << endl;
}